<?php
/**
 * Example with all available options:
 * array(
 *     "en" => array(   // Unique code for this template, use the language code (or language code with a number e.g de-1)
 *         "name" => "English",     // The name which shows up on the website
 *         "direction" => "ltr",    // Set toe rtl for right-to-left languages, you can ommit for ltr ones
 *         "single" => array(       // Settings for an email about a single product
 *             "credit" => "Thanks to George",
 *             "subject" => "Email Subject for Single Product Enquiry",
 *             "body" => "Email Body for Single Product Enquiry",
 *         ),
 *         "multi" => array(       // Settings for an email about all products from a company
 *             "credit" => "Thanks to George",
 *             "subject" => "Email Subject for Multiple Product Enquiry",
 *             "body" => "Email Body for Multiple Product Enquiry",
 *         ),
 *     )
 * )
 *
 * NOTE: Three variables are available: [YOUR NAME] [PRODUCT NAME] and [COMPANY NAME]
 * [BRAND NAME] as used in the current/old templates is no longer available.
 *
 */
$templates = array(
    "en" => array(
        "name" => "English",
        "single" => array(
            "credit" => "",
            "subject" => "Is [PRODUCT NAME] Vegan?",
            "body" => "Hi, I'm helping to update an online directory of vegan-friendly alcohol, and I was hoping you could provide some information about [COMPANY NAME].

Does [PRODUCT NAME] contain any animal ingredients (such as milk, eggs, honey, etc) or are animal products used in the processing/filtration of the product (such as isinglass, gelatin, etc)?

Also, is your product manufactured or bottled anywhere else in the world (by a sub-licensee, for instance) that might use a different processing system, thus making them unsuitable for vegans?

Thanks,

[YOUR NAME]"
        ), // End Single
        "multi" => array(
            "credit" => "",
            "subject" => "Are [COMPANY NAME] products vegan?",
            "body" => "Hi, I'm helping to update an online directory of vegan-friendly alcohol, and I was hoping you could provide some information about products produced by [COMPANY NAME].

Do any of your products contain any animal ingredients (such as milk, eggs, honey, etc) or are animal products used in the processing/filtration of any product (such as isinglass, gelatin, etc)?

Also, are your products manufactured or bottled anywhere else in the world (by a sub-licensee, for instance) that might use a different processing system, thus making them unsuitable for vegans?

Thanks,

[YOUR NAME]"
        ), // End Multi
    ), // End Language
    "fr" => array(
        "name" => "French",
        "single" => array(
            "credit" => "(Thanks to Monique for the French translation!)",
            "subject" => "",
            "body" => "Bonjour,

J'aide présentement à la mise à jour d'un répertoire en ligne de boissons alcoolisées végétaliennes et je me demande si vous pouviez me donner de l'information au sujet de vos produits ([PRODUCT 1, PRODUCT 2, PRODUCT 3.])

Est-ce que vos produits contiennent quelque ingrédient d'origine animale (comme du lait, des oeufs, du miel, etc.) ou est-ce qu'un quelconque produit d'origine animale est utilisé dans la fabrication/filtration des produits (par exemple : du ichtyocolle, de la gélatine, etc.)?

Je vous remercie.

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "es" => array(
        "name" => "Spanish",
        "single" => array(
            "credit" => "(Thanks to Georgina and Laura for the Spanish translation!)",
            "subject" => "Estoy [PRODUCT NAME] Vegano?",
            "body" => "Hola, estoy ayudando a actualizar una base de datos en el Internet sobre bebidas alcohólicas que los vegetarianos y los veganos pueden tomar y me preguntaba si usted me podría dar información sobre su [COMPANY NAME]

Su producto, [COMPANY NAME], ¿contiene ingredientes provenientes de los animales como leche, huevos, cualquier carne animal o, tal vez, miel? En el proceso de la filtración, ¿han usado otros productos provenientes de los animales como gelatina, huesos carbonizados (bone char) o ictiocola (que viene del pescado)? ¿Cuáles son los ingredientes de su [COMPANY NAME]?

Muchas gracias por su tiempo y le agradezco de antemano su respuesta.

Sinceramente,

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "da" => array(
        "name" => "Danish",
        "single" => array(
            "credit" => "(Thanks to Peter for the Danish translation!)",
            "subject" => "",
            "body" => "Goddag,

jeg hjælper med til at opdatere en online-guide til veganer-venlig alkohol, og jeg håber at I kan hjælpe med noget information vedrørende [PRODUCT NAME].

Indeholder [PRODUCT NAME] nogle animalske produkter (som f.eks. mælk, æg, honning eller lignende) eller indgår animalske produkter i fremstillingen/filtreringen af produktet (som f.eks. husblas, gelatine eller lignende)?

På forhånd tak,

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "de-1" => array(
        "name" => "German (version 1)",
        "single" => array(
            "credit" => "We've got two versions of the German question. This one's from Linda:",
            "subject" => "",
            "body" => "Sehr geehrte Damen und Herren,

ich helfe dabei, ein Verzeichnis fuer vegane Getraenke zu erstellen und zu aktualisieren. Hierfuer bitte ich Sie, mir einige Informationen ueber [BRAND OR PRODUCT NAME] zu geben.

Beinhaltet Ihr Getraenk tierische Zutaten (z.B. Milch, Ei, Honig)?
Werden während des Herstellungsprozesses tierische Produkte als Hilfsstoffe benutzt (z.B. Gelatine vom Schwein/Rind bzw. Hausenblase vom Fisch zur Filtration oder Aktivkohle auf tierischer Basis zur Schoenfaerbung von Zucker)?
Werden zur Etikettierung tierische Hilfsstoffe verwendet (z.B. kaseinhaltiger Leim)?

Vielen Dank für Ihre Unterstützung im Voraus.

Mit freundlichen Grüßen

[YOUR NAME]

Die Webseite mit den aufgelisteten Getraenken finden Sie unter www.barnivore.com"
        ), // End Single
    ), // End Language
    "de-2" => array(
        "name" => "German (version 2)",
        "single" => array(
            "credit" => "We've got two versions of the German question. This one's from Ingrid and Martina:",
            "subject" => "",
            "body" => "Sehr geehrte Damen und Herren,

ich helfe dabei ein Verzeichnis fuer veganische Lebensmittel zu erstellen. Hierfuer bitte ich Sie mir einige Informationen ueber Ihr Produkt zu geben.

Beinhaltet Ihr Bier/Wein/Alkohol tierische Zutaten (wie Milch, Eier, Honig usw.) oder werden bei der Herstellung tierische Produkte z.B. zur Filtration benutzt (Gelantine, Fischleim, Blut, oder andere tierische Produkte)?

Vielen Dank im Voraus.

Mit freundlichen Gruessen

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "pt" => array(
        "name" => "Portuguese",
        "single" => array(
            "credit" => "(Thanks to Tiago for the Portuguese translation!)",
            "subject" => "",
            "body" => "Olá. Estou ajudando a atualizar uma base de dados na internet sobre bebidas alcoólicas que vegetarianos e veganos podem consumir, e gostaria de saber se você poderia me fornecer informações sobre a [COMPANY NAME].

Seu produto, [COMPANY NAME], contém ingredientes de origem animal como leite, ovos, qualquer tipo de carne ou mel? No processo de filtração, são utilizados outros produtos provenientes de animais como gelatina, osso carbonizados (bone char) ou cola de peixe (isinglass - obtido do esturjão)?

Muito obrigado.

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "it" => array(
        "name" => "Italian",
        "single" => array(
            "credit" => "(Thanks to Marco for the Italian translation!)",
            "subject" => "",
            "body" => "Salve,

sto contribuendo all'aggiornamento di un elenco online di prodotti alcolici adatti a vegetariani e spero che possiate fornirmi qualche informazione a proposito di [PRODUCT NAME].

Vorrei sapere se [PRODUCT NAME] contiene ingredienti animali di qualche tipo (per esempio latte, uova, miele, ecc.), e se per la sua produzione vengono usati prodotti di derivazione animale per il trattamento o il filtraggio (per esempio caseina, colla di pesce/isinglass, gelatina, ecc.).

Grazie per la cortese attenzione,

cordiali saluti

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "he" => array(
        "name" => "Hebrew",
        "direction" => "rtl",
        "single" => array(
            "credit" => "(Thanks to Nir, Tal, and Sami for the Hebrew translation!)",
            "subject" => "",
            "body" => "שלום, אני חלק מצוות שמעדכן מאגר אינטרנטי של משקאות חריפים שמתאימים לטבעונים או צמחונים, וקיוויתי שתוכלו לספק לי מידע על [COMPANY NAME].

האם ב[COMPANY NAME] מצויים רכיבים מן החי (כגון חלב, ביצים, דבש וכד') או האם נעשה בהם שימוש בתהליכי עיבוד או סינון של המוצר (כגון איזינגלס, ג'לטין וכד')?

בנוסף, אשמח לדעת האם במקום אחר בעולם, יכול להיות שהמוצר שלכם מעובד בצורה שונה?

בתודה,

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "nl" => array(
        "name" => "Dutch",
        "single" => array(
            "credit" => "(Thanks to Avital, Carmen and Sami for the Dutch translation!)",
            "subject" => "",
            "body" => "Goedendag, op dit moment help ik een online bestand van veganistisch vriendelijke alcohol up to date te maken. Graag zou ik u willen vragen of u mij wat informatie kunt verstrekken over [COMPANY NAME].

Bevat [COMPANY NAME] dierlijke ingrediënten (zoals melk, eieren, honing enz. ) of zijn er dierlijke producten gebruikt in het maakproces of filtratie van het product ( zoals vislijm, gelatine, enz. )?

En is uw product ergens anders op de wereld vervaardigd ( bijvoorbeeld door een sublicentiehouder ) die het product op een andere manier zou kunnen produceren?

Graag hoor ik van u!

Met vriendelijke groet

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "sv" => array(
        "name" => "Swedish",
        "single" => array(
            "credit" => "(Thanks to David for the Swedish translation!)",
            "subject" => "",
            "body" => "Hej!

Jag hjälper till att fylla på en online-guide till vegan-vänlig alkohol och skulle uppskatta din hjälp rörande [COMPANY NAME].

Innehåller [COMPANY NAME] några animaliska produkter (exempelvis ägg, mjölk, honung osv) eller används animaliska produkter i framställningen/filtreringen av produkten (såsom husbloss, gelatin osv)?

Utöver detta så undrar jag även om produkten tillverkas på fler än en plats, och om tillverkningssätten kan skilja sig?

Tack på förhand,

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "fi" => array(
        "name" => "Finnish",
        "single" => array(
            "credit" => "(Thanks to Johanna for the Finnish translation!)",
            "subject" => "",
            "body" => "Hei!

Autan päivittämään internethakemistoa, johon on kerätty tietoa vegaanisista alkoholijuomista. Toivoisinkin, että voisitte tarjota lisätietoa [COMPANY NAME] tuotteista.

Sisältääkö [PRODUCT NAME] minkäänlaisia eläinperäisiä ainesosia (kuten maitoa, kananmunaa, hunajaa tms.) tai käytetäänkö eläinperäisiä ainesosia (esimerkiksi gelatiinia, kalaliimaa tai vastaavaa) [PRODUCT NAME] valmistamisessa tai suodattamisessa?

Lisäksi haluaisin tietää, että valmisetaanko [PRODUCT NAME] missään muualla maailmassa (esimerkiksi alihankkijasopimuksella), käyttäen erilaisia valmistusmenetelmiä?

Ystävällisin terveisin,

[YOUR NAME]"
        ), // End Single
    ), // End Language
    "no" => array(
        "name" => "Norwegian",
        "single" => array(
            "credit" => "(Thanks to Per for the Norwegian translation!)",
            "subject" => "",
            "body" => "Hei!

Jeg hjelper til med å fylle ut en guide til vegan-vennlig alkohol på nettet og håper du kan gi informasjon om [COMPANY NAME].

Inneholder [COMPANY NAME] noen animalske produkter (som feks. melk, egg, honning, osv.) eller blir animalske produkter brukt under fremstillingen eller filtreringen av produktet (som feks. husbloss/isinglass, gelatin, osv.)?

I tillegg lurer jeg på om produktet deres fremstilles andre steder, og om de kanskje gjør det på en annen måte enn dere?

På forhånd takk,

[YOUR NAME]"
        ), // End Single
    ), // End Language
);

asort($templates);
$json = json_encode($templates);
file_put_contents(__DIR__.'/public/compose-email-templates.json', $json);
