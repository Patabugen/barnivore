
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <title>Contact us</title>
  <meta name="Description" content="How to get in touch with us." />
  <link href="assets/application.css" media="screen" rel="stylesheet" />
  <script src="assets/application.js"></script>
</head>
<body class="static contact">

<div id = "header">
  <a href = "/"><img src = "images/logo-300x90.png" width="300" height="90" alt = "Barnivore, your vegan wine and beer guide" /></a>

  <div id = "mainmenu">
    <ul>
      <li class = "first"><a href = "/beer">Beer</a></li>
      <li><a href = "http://barnivore.com/wine">Wine</a></li>
      <li><a href = "http://barnivore.com/liquor">Liquor</a></li>
      <li><a href = "http://barnivore.com/askacompany">Ask a Company</a></li>
      <li><a href = "http://barnivore.com/mobile">Mobile Apps</a></li>
      <li><a href = "http://barnivore.com/contact">Contact</a></li>
      <li><a href = "http://barnivore.com/faq">FAQ</a></li>
    </ul>
  </div>
</div>
<div id = "wrapper">

  <div id = "searchbox-mobile">
    <form action="/search" method="get">
        <label for="keyword">Find Booze:</label>
        <input name = "keyword" id = "keyword" value = "" class="search" />
        <input type="submit" value="Search" />
    </form>
  </div>

  <div id = "right">

    <div id = "searchbox">
      <form action="/search" method="get">
          <label for="keyword">Find Booze:</label>
          <input name = "keyword" id = "keyword" value = "" class="search" />
          <input type="submit" value="Search" />
      </form>
    </div>

    <div id = "adbox">
    </div>

    <div id = "social">
      <iframe src="about:blank" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:336px; height:427px;" allowTransparency="true"></iframe>
    </div>

  </div>

  <div id = "content">
