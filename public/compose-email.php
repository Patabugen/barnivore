<?php require(__DIR__.'/header.php');?>

<link href="jquery.barnivore-composer.css" media="screen" rel="stylesheet" />
<script src="jquery.barnivore-composer.js"></script>
<p style='font-weight: bold; color: red; font-size: 1.2em;'>
This is a sample form created by <a href='http://patabugen.co.uk'>Sami Greenbury</a> and is not (yet..) endorsed by - or even seen by Barnivote. I've 
copied their layout to make it easier for them to (hopefully) intergrate! If your name is Jason, please check your inbox ;)
</p>
<h1>Enquiry Builder</h1>
<p>
Use this form to generate a template email which you can send to a company to enquiry about the veganosity of their products.
</p>
<p>
Please follow the guidelines here when contacting a company: 
<a href='http://www.barnivore.com/askacompany'>http://www.barnivore.com/askacompany</a>
</p>
<form id='compose_email_options'>
    <h2>Options</h2>
    <div class='form-group'>
        <label for='compose_email_language'>
            Email Language
        </label>
        <select id='compose_email_language' name='language'>
            <option value='en' selected="selected">English</option>
            <option value='es'>Spanish</option>
        </select>
    </div>
    <div class='form-group'>
        <input type='checkbox' id='compose_email_single_product' name='single_product' checked="checked">
        <label for='compose_email_single_product'>
            Single Product Enquiry
        </label><br/>
        <small>(un-check if you want to ask a company about every product they make, not available for every language)</small>
    </div>
    <div id='compose_email_single_product_fields'>
        <label for='compose_email_product_name'>
            Product Name
        </label>
        <input type='text' id='compose_email_product_name' name='product_name'>
    </div>
    <div class='form-group'>
        <label for='compose_email_company_name'>
            Company Name
        </label>
        <input type='text' id='compose_email_company_name' name='company_name'>
    </div>
    <div class='form-group'>
        <label for='compose_email_your_name'>
            Your Name
        </label>
        <input type='text' id='compose_email_your_name' name='your_name'>
    </div>
</form>
<div style='display: none' id='compose_email_error'></div>
<form id='compose_email_results'>
    <h2>Results</h2>
    <div class='form-group'>
        <label for='compose_email_subject'>
            Subject
        </label>
        <input type='text' id='compose_email_subject' name='subject'>
    </div>
    <div class='form-group'>
        <label for='compose_email_body'>
            Body
        </label>
        <textarea id='compose_email_body' name='body'></textarea>
    </div>
    <div class='compose_email_credit'></div>
</form>

<script>
(function ( $, window, document, undefined ) {
    jQuery(function($){
        $('#compose_email_options').barnivoreComposer({
            optionsForm: '#compose_email_options',
            resultsForm: '#compose_email_results',
            jsonData: 'compose-email-templates.json',
            defaultLanguage: 'en',
            onDone: function(options) {
                $('#compose_email_error').slideUp().text('');
                $('#compose_email_results').slideDown();
            },
            onError: function(error, options) {
                $('#compose_email_error').text(error).slideDown();
                $('#compose_email_results').hide();
            }
        });
    });
})(jQuery, window, document);
</script>

<?php require(__DIR__.'/footer.php');
