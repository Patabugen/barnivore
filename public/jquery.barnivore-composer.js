/*!
 * Barnivore Composer
 * Original author: Sami Greenbury <contact@patabugen.co.uk>
 * Based on jQuery Plugin Boilerplate: https://github.com/jquery-boilerplate
 * Licensed under the MIT license
 */

;(function ( $, window, document, undefined ) {
    // Create the defaults once
    var pluginName = "barnivoreComposer",
        defaults = {
            onError: function(){},
            onDone: function(){},
            defaultLanguage: 'en',
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        this.element = element;
        this.optionsForm;
        this.resultForm;
        this.templateData;
        // Timer to delay processing changes for a few ms
        this.updateTimer = undefined;

        // jQuery has an extend method that merges the
        // contents of two or more objects, storing the
        // result in the first object. The first object
        // is generally empty because we don't want to alter
        // the default options for future instances of the plugin
        this.options = $.extend( {}, defaults, options) ;

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    Plugin.prototype = {

        init: function() {
            self = this;
            // Lookup and save a reference to elements we will use often
            self.optionsForm = $(self.options.optionsForm);
            self.resultsForm = $(self.options.resultsForm);
            self.resultsFormSubject = self.resultsForm.find('[name="subject"]');
            self.resultsFormBody = self.resultsForm.find('[name="body"]');
            self.resultsFormCredit = self.resultsForm.find('.compose_email_credit');

            // Load the JSON data
            $.get(this.options.jsonData, function(data){
                self.templateData = data;
                // Add the known languages to the select box
                self.loadLanguageOptions(self.optionsForm, data);
            });

            // Bind to changes in the Options form
            self.optionsForm.find('input, select').on('keypress change mouse-up', function() {
                // Clear the form
                self.resultsFormSubject.val('');
                self.resultsFormBody.val('');
                self.resultsFormCredit.text('');


                clearTimeout(this.updateTimer);
                this.updateTimer = setTimeout(function(){
                    var options = self.getSelectedOptions(self.optionsForm);
                    var template = self.getTemplate(options.language, options.single_product);
                    if (!template) {
                        self.options.onError.call(self, self.error, options);
                        return;
                    }
                    template = self.fillTemplate(template, options);
                    self.resultsFormSubject.val(template.subject);
                    self.resultsFormBody.val(template.body);
                    if (template.credit) {
                        self.resultsFormCredit.text(template.credit);
                    }
                    if (template.direction) {
                        self.resultsFormSubject.prop('dir', template.direction);
                        self.resultsFormBody.prop('dir', template.direction);
                    } else {
                        self.resultsFormSubject.prop('dir', 'ltr');
                        self.resultsFormBody.prop('dir', 'ltr');
                    }
                    self.options.onDone.call(self);
                }, 300);

            }).trigger('change');
            self.optionsForm.find('input[name="single_product"]').on('change', function(){
		self.optionsForm.find('input[name="product_name"]').prop('disabled', !$(this).is(':checked'));
	    });
        },

        loadLanguageOptions: function (optionsForm, data) {
            var select = optionsForm.find('[name="language"]');
            select.children().remove();
            for (language in data) {
                if (data.hasOwnProperty(language)) {
                    var code = language;
                    var name = data[language].name;
                    var option = $('<option>');
                    option.val(code);
                    option.text(name);
                    if (code == this.options.defaultLanguage) {
                        option.prop('selected', 'selected');
                    }
                    option.appendTo(select);
                }
            }
        },

        getSelectedOptions: function(optionsForm)
        {
            return {
                language:       optionsForm.find('select[name="language"] option:selected').val(),
                single_product: optionsForm.find('input[name="single_product"]').is(':checked'),
                product_name:   optionsForm.find('input[name="product_name"]').val(),
                company_name:   optionsForm.find('input[name="company_name"]').val(),
                your_name:      optionsForm.find('input[name="your_name"]').val(),
            }
        },

        getTemplate: function(language, single_product)
        {
            // Check the language exists
            if (this.templateData[language] == undefined) {
                this.error = "No templates found for language: " + language;
                return false;
            }
            var languageName = this.templateData[language].name;
            // If we're single, check the single product template exists
            if (single_product) {
                if (this.templateData[language].single == undefined) {
                    this.error = "No single product template found for " + languageName;
                    return false;
                }
                return {
                    subject: this.templateData[language].single.subject,
                    body: this.templateData[language].single.body,
                    credit: this.templateData[language].single.credit,
                    direction: this.templateData[language].direction,
                };
            } else {
                if (this.templateData[language].multi == undefined) {
                    this.error = "No multi-product template found for " + languageName;
                    return false;
                }
                return {
                    subject: this.templateData[language].multi.subject,
                    body: this.templateData[language].multi.body,
                    credit: this.templateData[language].multi.credit,
                    direction: this.templateData[language].direction,
                };
            }
        },

        fillTemplate: function(template, options) {
            if (options.product_name.length > 0) {
                template.body = template.body.replace(/\[PRODUCT NAME\]/g, options.product_name);
                template.subject = template.subject.replace(/\[PRODUCT NAME\]/g, options.product_name);
            }

            if (options.company_name.length > 0) {
                template.body = template.body.replace(/\[COMPANY NAME\]/g, options.company_name);
                template.subject = template.subject.replace(/\[COMPANY NAME\]/g, options.company_name);
            }

            if (options.your_name.length > 0) {
                template.body = template.body.replace(/\[YOUR NAME\]/g, options.your_name);
                template.subject = template.subject.replace(/\[YOUR NAME\]/g, options.your_name);
            }
            return template;
        }
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );
